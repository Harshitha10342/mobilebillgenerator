package com.MobileBillGenerator.fileread;

import java.io.IOException;
import java.util.*;

/**
 * This class contains the login method to allow a admin to login
 * 
 * @author RANJHS
 */

public class AdminLogin {

	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\AdminDetails2.txt";

	static ArrayList<Admin> list = new ArrayList<Admin>();

	/**
	 * This methods allowss admin to login
	 * 
	 * @author RANJHS
	 * @throws IOException
	 */
	public static void login() throws IOException {
		AdminRegistration.users();
		list = AdminRegistration.getList();
		int count = 0;
		int count1 = 0;
		int count2 = 0;

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		while (count1 < 3) {
			System.out.println("Enter Username:");
			String username = sc.next();
			System.out.println("Enter Password:");
			String password = sc.next();
			Iterator<Admin> i = list.iterator();
			while (i.hasNext()) {
				Admin a = (Admin) i.next();

				if (username.equals(a.getUserName())) {
					count1 = 4;
					while (count < 3) {
						if (password.equals(a.getPassword())) {
							count = 3;
							System.out.println("Logged in SuccessFully");
							break;
						} else {

							System.out.println("Enter the correct password");
							count++;
							password = sc.next();
						}

						if (count == 3) {
							System.out.println("Do you want to reset your password\nyes or no");
							String reset = sc.next();
							if (reset.equalsIgnoreCase("yes")) {
								while (count2 < 3) {
									System.out.println("Answer the security question " + a.getSecurityQuestion());
									String answer = sc.next();
									if (answer.equals(a.getSecurityAnswer())) {
										count2 = 4;
										System.out.println("Enter your new password:");
										String pwd = sc.next();
										a.setPassword(pwd);
										AdminRegistration.setList(list);
										System.out.println("Your password got reset");
									} else {
										System.out.println("Enter the correct security answer");
										count2 = count2 + 1;
									}
								}
								if (count2 == 3) {
									System.out.println("do yoy want to reset your account \n yes or no");
									String reset1 = sc.next();
									if (reset1.equalsIgnoreCase("yes")) {
										AdminRegistration.delete(username);
										AdminRegistration.newAdmin();
										
									} else {
										System.out.println("please try after some time ");
										System.exit(0);
									}
								}
							}

						}
					}
				}

			}

			if (count1 < 3) {
				System.out.println("wrong user name entered ");
				count1++;
			}

		}
		if (count1 == 3) {
			System.out.println("do yoy want to reset your account \n yes or no");
			String reset1 = sc.next();
			if (reset1.equalsIgnoreCase("yes")) {
				AdminRegistration.newAdmin();
			} else {
				System.out.println("please try after some time ");
				System.exit(0);
			}
		}
	}



}
