package com.MobileBillGenerator.fileread;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;
/**
*  This class registers a new  admin 
* @author RANJHS
*/



public class AdminRegistration {

	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\AdminDetails2.txt";

		 static Admin newadmin = new Admin();
		
		static int count=0;
		static String username;
		static String password;
		static String reEnter_pwd;
		
		static ArrayList<Admin> list=new ArrayList<Admin>();
		/**
		*  This method contains the registered  admins 
		* @author RANJHS
		*/
		
		public static  void users() throws IOException {
			File f=new File(fileName);
			if(f.length()==0)
			{
				FileOutputStream	fo = new FileOutputStream(fileName);
				ObjectOutputStream os=new ObjectOutputStream(fo);
				os.writeObject(list);
				os.close();
			}
			list=getList();
			if(list.size()<1) {
			Admin a=new Admin();
			a.setUserName("naidunama");
			a.setPassword("naidu@123");
			a.setSecurityQuestion("What is your Favourite Dish?");
			a.setSecurityAnswer("fish");
			list.add(a);
			setList(list);
			} else {
				
			}
		}
		
		/**
		 *  This method registers a new  admin 
		 * @return true if admin registration success...else false
		 * @throws IOException
		 */
		
		public static  boolean adminRegistration() throws IOException {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			boolean validUsername ;
			int userCount=0;
			int passwordCount=0;
			int flag=1;
			
			Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");
			
				list=getList();
				while(count<3 ){
				System.out.println("enter user name:");
				username = sc.next();
				Iterator<Admin> itr = list.iterator();
				while (itr.hasNext()) {
				    Admin element = itr.next();
				    if(username.equals(element.getUserName()))
				    {
				    	System.out.println("Sorry You already existed");
				    	flag=0;
				    	break;
				    }
				  
				}
				if(flag==0)
				{
					flag=1;
					continue;
				}
				validUsername = (username != null) && pattern.matcher(username).matches();
				if(username.length()<8)
				{
					if(userCount==2)
					{
						System.out.println("Sorry You more than 3 times entered incorrect please trying latter");
						System.exit(0);			
					}
						userCount++;
						System.out.println("username enter more than 8 characters");
						continue;
				}
				if( !validUsername )
				{
					if(userCount==2)
					{
						System.out.println("Sorry You more than 3 times entered incorrect please trying latter");
						System.exit(0);	
					}
					userCount++;
					System.out.println("Sorry You entered wrong details");
					continue;
					
				}
				newadmin.setUserName(username);
				
				while(passwordCount<3)
				{
					System.out.println("enter password:");
					password = sc.next();
					if(password.length()<8)
					{
						++passwordCount;
						if(passwordCount==3)
						{
							System.out.println("Sorry You entered wrong credential");
							passwordCount=0;
							System.exit(0);
						}
						System.out.println("password more than 8 characters");
					}
					else
					{
						System.out.println("re-enter password:");
						reEnter_pwd = sc.next();
						if (password.equals(reEnter_pwd)) {
							newadmin.setPassword(password);
							System.out.println("Choose a Security Question");
							System.out.println("1.Who is your Favourite Actor?");
							System.out.println("2.What is your Favourite Dish?");
							System.out.println("3.What is your Favourite Place?");
							System.out.println("4.What is your Favourite Sport?");
							int questionChoice = sc.nextInt();
							String securityAns;
							String securityQuestion;
							switch (questionChoice) {
							case 1:
								securityQuestion = "Who is your Favourite Actor?";
								System.out.println(securityQuestion);
								newadmin.setSecurityQuestion(securityQuestion);
								securityAns = sc.next();
								newadmin.setSecurityAnswer(securityAns);
								break;
							case 2:
								securityQuestion = "What is your Favourite Dish?";
								System.out.println(securityQuestion);
								newadmin.setSecurityQuestion(securityQuestion);
								securityAns = sc.next();
								newadmin.setSecurityAnswer(securityAns);
								break;
							case 3:
								securityQuestion = "What is your Favourite Place?";
								System.out.println(securityQuestion);
								newadmin.setSecurityQuestion(securityQuestion);
								securityAns = sc.next();
								newadmin.setSecurityAnswer(securityAns);
								break;
							case 4:
								securityQuestion = "What is your Favourite Sport?";
								System.out.println(securityQuestion);
								newadmin.setSecurityQuestion(securityQuestion);
								securityAns = sc.next();
								newadmin.setSecurityAnswer(securityAns);
								break;
								default:System.out.println("enter correct input");
							}
							list.add(newadmin);
							
							//list=getList();
							setList(list);
							list=getList();
							break;
						
						} else {
							if(count==3)
							{
								System.out.println("You've exceeded your limit,try again after sometime");
								System.exit(0);
							}
								System.out.println("your password doesn't match try again");
								count++;
								
								continue;
								
							}
						
					}
					
				}
				
				return true;
				
				
				
			}
				
				return false;
			
		}
		/**
		 * this method sets the admin data into files
		 * @param list
		 */
		public static void setList(ArrayList<Admin> list)
		{
			FileOutputStream fos=null;
			ObjectOutputStream oos=null;
			try {
				for(int i=0;i<list.size();i++) {
				fos = new FileOutputStream(fileName);
				oos=new ObjectOutputStream(fos);
				oos.writeObject(list);
				
				}
				oos.close();
				fos.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		/**
		 * this method deletes admin details if the admin want to reset his account 
		 */
		public static void delete(String username) {
			list=getList();
			for(int i=0;i<list.size();i++) {
			    if(username.equals(list.get(i).getUserName()))
			    {
			    	list.remove(i);
			    }
			}
			setList(list);
			
	
		}
		/**
		 * this method reads the admin login information from the file
		 * @return list
		 */
		@SuppressWarnings("unchecked")
		public static  ArrayList<Admin> getList()
		{
			
			ArrayList<Admin> list1=new ArrayList<Admin>();
			FileInputStream fis;
			try {
				fis = new FileInputStream(fileName);
				ObjectInputStream ois=new ObjectInputStream(fis);
				list1=(ArrayList<Admin>) ois.readObject();
				ois.close();
				fis.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
			return list1;
		}
/**
 * this method calls admin registration method to register the new admin
 * @throws IOException
 */
		
			 public static void newAdmin() throws IOException {
				 list=getList();
				 System.out.println(list);
			if (adminRegistration()) {
				System.out.println("regsitered Successfully");
			} else {
				System.out.println("You've exceeded your limit,try after sometime!!");
			}

		}
		
	}


