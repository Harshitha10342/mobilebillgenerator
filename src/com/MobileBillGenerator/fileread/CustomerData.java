package com.MobileBillGenerator.fileread;

//importing the predefined packages
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
* This class checks the user belongs to our network or not
* @author RANJH
*/

public class CustomerData {

	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\CustomerData.txt";

	/**
	 * it reads a file based on the parameter of mobilenumber
	 * @return it return true if mobilenumber belongs to our network ....else false
	 * @throws IOException
	 */

	public static boolean mobileNetworkValidation(String mobileNumber) throws IOException {

		String line;
		File file = new File(fileName);
		BufferedReader bufferreader = null;
		// this block handles exception when file not found
		try {

			bufferreader = new BufferedReader(new FileReader(file));
			while ((line = bufferreader.readLine()) != null) {

				// if given mobilenumber matches with the mobilenumber in the database
				if (line.equals(mobileNumber)) {
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println("File not found..Enter the correct file path!!");
			System.exit(0);

		} finally {
			bufferreader.close();
		}
		return false;
	}
}
