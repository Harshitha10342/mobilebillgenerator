package com.MobileBillGenerator.fileread;


/**
 * this class is used for declaring all the getters and setters method and
 * implements serializable interface
 * 
 * @author RANJHS
 *
 */
public class RegisteredUsersData implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String mobile;
	private String network;
	private String customerName;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	RegisteredUsersData(String mobile, String network, String customerName) {
		this.network = network;
		this.mobile = mobile;
		this.customerName = customerName;
	}

	// toString method to display the data
	@Override
	public String toString() {
		return "mobile=" + mobile + ", network=" + network + ",Name=" + customerName;
	}

}