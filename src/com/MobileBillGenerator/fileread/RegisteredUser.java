package com.MobileBillGenerator.fileread;


import java.io.File;
//importing the Pre-defined packages
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
* this class is used for the new users to register
* 
* @author RANJHS
*
*/
public class RegisteredUser {

	static  List<RegisteredUsersData> list = new ArrayList<RegisteredUsersData>();
	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\registereduser.txt";
	
	/**
	 * this method checks whether the user is already registered or not
	 * 
	 * @param mobileNumber it checks with the mobile number which is entered by the
	 *                     user
	 * @return if true return prepaid or postpaid, if false it returns -1
	 */
	public static String readFileContent(String mobileNumber) {
		// Deserialization
		list = getRegister();
		try {
			// Reading the object from a file
			// System.out.println(list);
			Iterator<RegisteredUsersData> i = list.iterator();
			while (i.hasNext()) {
				RegisteredUsersData e = i.next();
				if (mobileNumber.equals(e.getMobile())) {
					return e.getNetwork();
				}
			}

		} catch (Exception ex) {
			System.out.println("IOException is caught");
		}
		return "-1";
	}

	/**
	 * this method creates the user objects and stores the data(hardcode values)
	 * @throws IOException 
	 */
	public static void users() throws IOException {
		File f=new File(fileName);
		if(f.length()==0) {
		FileOutputStream fileOut = new FileOutputStream(f);
		@SuppressWarnings("resource")
		ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
		objOut.writeObject(list);
		}
		list = getRegister();
			RegisteredUsersData ser = new RegisteredUsersData("9700401589", "prepaid", "anu");
			RegisteredUsersData ser1 = new RegisteredUsersData("9177995290", "prepaid", "harshitha");
			RegisteredUsersData ser2 = new RegisteredUsersData("9502199202", "postpaid", "supriya");
			RegisteredUsersData ser3 = new RegisteredUsersData("9949730470", "prepaid", "ramu");
			RegisteredUsersData ser4 = new RegisteredUsersData("9121719350", "postpaid", "jyothi");
			RegisteredUsersData ser5 = new RegisteredUsersData("7729955884", "prepaid", "naidu");

			// add the data into the list
			list.add(ser);
			list.add(ser1);
			list.add(ser2);
			list.add(ser3);
			list.add(ser4);
			list.add(ser5);
			setRegister(list);
		
	}

	/**
	 * this method helps the new user to register into the application 
	 * @param sc it reads the input from the keyboard
	 * @param mobileNumberInput this is user entered mobile number
	 */
	public static String register( String mobileNumberInput) {
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		list = getRegister();
		System.out.println("enter ur name:");
		String customerName = sc.next();
		System.out.println("choose ur operator type:prepaid or postpaid");
		String network = sc.next();
		RegisteredUsersData newUser = new RegisteredUsersData(mobileNumberInput, network, customerName);
		list.add(newUser);

		setRegister(list);
		list = getRegister();
		return network;
	}

	/**
	 * 
	 * @param list It is the ArrayList which stores the objects in which data is
	 *             written into the file
	 */
	public static void setRegister(List<RegisteredUsersData> list) {
		FileOutputStream file = null;
		ObjectOutputStream out = null;
		try {

			for (int i = 0; i < list.size(); i++) {
				file = new FileOutputStream(fileName);
				out = new ObjectOutputStream(file);
				out.writeObject(list);
			}
			// close all the opened resources
			out.close();
			file.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")

	/**
	 * this method is used to read/retrieve the data from the file
	 * 
	 * @return it returns the user data which is stored in the list
	 */
	public static List<RegisteredUsersData> getRegister() {
		List<RegisteredUsersData> list2 = new ArrayList<RegisteredUsersData>();
		try {
			// Saving of object in a file
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(file);
			// read the data from the list
			list2 = (List<RegisteredUsersData>) in.readObject();
			// close all the opened resources
			in.close();
			file.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("IOException is caught");
		}
		return list2;

	}

}
