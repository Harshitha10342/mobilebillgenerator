package com.MobileBillGenerator.main;

//importing the Pre-defined packages
import java.io.IOException;
import java.util.Scanner;

import com.MobileBillGenerator.billPlans.PostPaidBillPlans;
import com.MobileBillGenerator.billPlans.PrePaidBillPlans;
import com.MobileBillGenerator.fileread.AdminLogin;
import com.MobileBillGenerator.fileread.AdminRegistration;
import com.MobileBillGenerator.fileread.RegisteredUser;
import com.MobileBillGenerator.login.AdminAccess;
import com.MobileBillGenerator.login.LoginUtilities;



/**
*  This class contains the main method to allow a user to login
* @author RANJHS
*/

public class Main {

	/**
	 * This method makes the call to doLogin to allow a user to login
	 * @param args no args are taken
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// initialize the scanner class
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your choice");
		System.out.println("1.Admin");
		System.out.println("2.Admin Registration");
		System.out.println("3.Users");
		int choice=0;
		try {
		choice= sc.nextInt();
		}catch(Exception e) {
			System.out.print("The input is of not integer ");
			
		}
		
		switch(choice) {
		
		case 1:
			
			AdminLogin.login();
			AdminAccess.adminAccess();
			
				break;
		case 2:AdminRegistration.newAdmin();
			break;
		case 3:RegisteredUser.users();
		// login a user
			(new LoginUtilities()).doLogin();

			if ((LoginUtilities.network).equals(LoginUtilities.PREPAID_STRING)) {
				PrePaidBillPlans.prepaidPlans();
			} else if ((LoginUtilities.network).equals(LoginUtilities.POSTPAID_STRING)) {
				PostPaidBillPlans.postpaidPlans();
			}
		break;
		default:
			System.out.println("wrong input entered ");
			
		}
		
	
	}
}