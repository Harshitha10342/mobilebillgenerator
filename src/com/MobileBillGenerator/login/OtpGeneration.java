package com.MobileBillGenerator.login;

//importing the predefined packages
import java.util.Random;
import java.util.Scanner;

/**
 * This class generates the OTP for Registered Users
 * 
 * @author RANJH
 *
 */
public class OtpGeneration {
	/**
	 * This method generates an OTP
	 * 
	 * @param sc scanner class object to take input from the user
	 * @return true if user enters correct OTP,false if user doesn't enter correct
	 *         OTP
	 */

	public static boolean otpGen() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		int count = 1;

		// declaring the predefined Random class object for generating OTP
		Random rd = new Random();
		int otp = rd.nextInt(10000);
		System.out.println(otp);
		if (otp < 0) {
			otp = otp * (-1);
		}

		// entered OTP is correct
		System.out.println("enter the otp");

		int enterOtp = sc.nextInt();

		if (enterOtp == otp) {
			flag = true;
			count = 3;

		}
		// entered OTP is wrong, iterates for 2 times
		while (count != 3) {
			System.out.println("wrong entered otp!!! re-enter for " + count + " time");
			enterOtp = sc.nextInt();
			// if user enters correct OTP
			if (enterOtp == otp) {
				flag = true;
				break;
			}
			flag = false;
			count = count + 1;

		}

		// returns true or false based on the input(OTP) given by the user
		return flag;

	}
}
