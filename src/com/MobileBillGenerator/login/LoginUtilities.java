package com.MobileBillGenerator.login;


import java.io.IOException;
//importing the Pre-defined packages
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.MobileBillGenerator.fileread.CustomerData;
import com.MobileBillGenerator.fileread.RegisteredUser;




/**
* This method  doLogin to allow a user to login
* @author RANJHS
*
*/
public class LoginUtilities {
	
	static final String VALIDATION_PATTERN = "(0/91)?[7-9][0-9]{9}";
	public static final String PREPAID_STRING = "prepaid";
	public static final String POSTPAID_STRING = "postpaid";
	static final String ENTERED_MOBILENUMBER="Please enter your registered mobile number:";
	public static String network; 
	public static String mobileNumberInput ;
	/** 
	 * method for checking the credentials of the users mobile number
	 * @param sc the global scanner object to read user input
	 * @throws IOException 
	 */
	public void doLogin() throws IOException {
		Scanner sc = new Scanner(System.in);
		int attemptsCount = 0;
		attemptsloop: while (attemptsCount < 4) {
			
			System.out.println(ENTERED_MOBILENUMBER);
			mobileNumberInput = sc.next();
			
			if (mobileNumberValidation(mobileNumberInput)) {
				// mobile number is valid
				
				//checkks the customer belongs to our network
				if(CustomerData.mobileNetworkValidation(mobileNumberInput)) {
				
				// find corresponding network-operator for this mobileNumberInput
				network= RegisteredUser.readFileContent(mobileNumberInput);
				switch (network) {
					case PREPAID_STRING:
					case POSTPAID_STRING:
						//generate the OTP, because this is a valid mobile number
						if (OtpGeneration.otpGen()) {
							System.out.println("Login successful");
							System.out.println("Mobile Number = " + mobileNumberInput);
							
						} else {
							System.out.println("Login failed, due to wrong entry of otps...please try again!!");
						}
						// TODO: NEXT STEP HERE
						
						break;
					default:
						System.out.println("user was not registered,please register with ur mobile number again");
						network=RegisteredUser.register(mobileNumberInput);
						if (OtpGeneration.otpGen()) {
							System.out.println("Login successful");
							System.out.println("Mobile Number = " + mobileNumberInput);
							System.out.println("Registration succesfully");
							
						} else {
							System.out.println("Login failed, due to wrong entry of otps...please try again!!");
						}
						
						break;
				}
				
				// break the while loop because the number is valid
				break attemptsloop;
			} else {
				System.out.println("the mobile number u entered not belongs to our network");
				System.exit(0);} 
			} else {
				// number entered is invalid 
				attemptsCount++;
				if (attemptsCount != 4) {
					System.out.println("the entered number is wrong,please renter. Attempt no." + attemptsCount);
				}
			}
		}
		
		if (attemptsCount == 4) {
			System.out.println("Too many attempts, please try after some time");
			// TODO next steps here
		}
		
	} 
	
	/** 
	 * it validates the mobile number
	 * @param mobileNumber the mobile number input by the user
	 * @return true if the number is valid, false if it is invalid
	 */
	static boolean mobileNumberValidation(String mobileNumber)
	{
		// creating a matcher to validate the input number
		Matcher m = Pattern.compile(VALIDATION_PATTERN).matcher(mobileNumber);
		
		// return true if the match is successful, else false
		return (m.find() && m.group().equals(mobileNumber));
	}
}








