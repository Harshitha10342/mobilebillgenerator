package com.MobileBillGenerator.login;

import java.util.Scanner;

import com.MobileBillGenerator.plans.PostpaidPacks;
import com.MobileBillGenerator.plans.PrepaidDatapacks;
import com.MobileBillGenerator.plans.PrepaidSpecialpacks;
import com.MobileBillGenerator.plans.PrepaidTalktimePlans;
import com.MobileBillGenerator.plans.PrepaidUnlimitedplans;

/**
 * the class allows the admin to do certain operations
 * 
 * @author ranjhs
 *
 */
public class AdminAccess {
	/**
	 * the method update the plans and send the bills to customers
	 */

	public static void adminAccess() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("1.Plans display and Updation ");
		System.out.println("2.Postpaid bill calculation ");
		System.out.println("3.Logout");

		int choice1 = 0;
		try {
			choice1 = sc.nextInt();
		} catch (Exception e) {
			System.out.print("the input is of not integer");
		}
		switch (choice1) {
		case 1:
			System.out.println("1.Prepaidunlimitedplans ");
			System.out.println("2.Prepaidtalktimeplans ");
			System.out.println("3.Prepaiddataplans ");
			System.out.println("4.Prepaidspecialplans ");
			System.out.println("5.Postpaidplans ");
			System.out.println("0.Back");

			int choice2 = 0;
			try {
				choice2 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
			}
			switch (choice2) {
			case 1:
				System.out.println("1.AddPrepaidUnlimitedplans ");
				System.out.println("2.DisplayPrepaidUnlimitedplans");
				System.out.println("3.UpdatePrepaidUnlimitedplans");
				System.out.println("0.Back");
				int choicePrepaidUnlimitedPlans = 0;
				try {
					choicePrepaidUnlimitedPlans = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				}
				switch (choicePrepaidUnlimitedPlans) {
				case 1:
					PrepaidUnlimitedplans.addUnlimitedPlans();
					break;
				case 2:
					PrepaidUnlimitedplans.displayUnlimited();
					break;
				case 3:
					PrepaidUnlimitedplans.updateUnlimitedPlans();
					break;
				case 0:
					adminAccess();
					break;
				default:
					System.out.println("wrong input entered ");
					adminAccess();
					break;
				}
				break;
			case 2:

				System.out.println("1.AddPrepaidTalktimeplans ");
				System.out.println("2.DisplayPrepaidTalktimeplans");
				System.out.println("3.UpdatePrepaidTalktimeplans");
				System.out.println("0.Back");
				int choicePrepaidTalktimePlans = 0;
				try {
					choicePrepaidTalktimePlans = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				}
				switch (choicePrepaidTalktimePlans) {
				case 1:
					PrepaidTalktimePlans.addTalkTimePlans();
					break;
				case 2:
					PrepaidTalktimePlans.displayTalktimePlans();
					break;
				case 3:
					PrepaidTalktimePlans.updateTalktimePlans();
					break;
				case 0:
					adminAccess();
					break;
				default:
					System.out.println("wrong input entered ");
					adminAccess();
					break;

				}
				break;
			case 3:
				System.out.println("1.AddPrepaidDataplans ");
				System.out.println("2.DisplayPrepaidDataplans");
				System.out.println("3.UpdatePrepaidDataplans");
				System.out.println("0.Back");
				int choicePrepaidDataPlans = 0;

				try {
					choicePrepaidDataPlans = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				}
				switch (choicePrepaidDataPlans) {
				case 1:
					PrepaidDatapacks.addDatapacks();
					break;
				case 2:
					PrepaidDatapacks.displayDatapack();
					break;
				case 3:
					PrepaidDatapacks.updateDataPlans();
					break;
				case 0:
					adminAccess();
					break;
				default:
					System.out.println("wrong input entered ");
					adminAccess();
					break;

				}
				break;
			case 4:

				System.out.println("1.AddPrepaidSpecialplans ");
				System.out.println("2.DisplayPrepaidSpecialplans");
				System.out.println("3.UpdatePrepaidSpecialplans");
				System.out.println("0.Back");
				int choicePrepaidSpecialPlans = 0;

				try {
					choicePrepaidSpecialPlans = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				}
				switch (choicePrepaidSpecialPlans) {
				case 1:
					PrepaidSpecialpacks.addSpecialpackPlans();
					break;
				case 2:
					PrepaidSpecialpacks.displaySpecialpack();
					break;
				case 3:
					PrepaidSpecialpacks.updateSpecialpackPlans();
					break;
				case 0:
					adminAccess();
					break;
				default:
					System.out.println("wrong input entered ");
					adminAccess();
					break;

				}
				break;
			case 5:
				System.out.println("1.AddPostpaidplans ");
				System.out.println("2.DisplaypostPaidplans");
				System.out.println("3.Updatepostpaidplans");
				System.out.println("0.Back");
				int choiceUpdatePostPaidPlans = 0;

				try {
					choiceUpdatePostPaidPlans = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				}
				switch (choiceUpdatePostPaidPlans) {
				case 1:
					PostpaidPacks.addPostpaidPlans();
					break;
				case 2:
					PostpaidPacks.displayPostpaid();
					break;
				case 3:
					PostpaidPacks.updatePostpaidPlans();
					break;
				case 0:
					adminAccess();
					break;
				default:
					System.out.println("wrong input entered ");
					adminAccess();
					break;
				}
				break;
			case 0:
				adminAccess();
				break;
			default:
				System.out.println("wrong input entered ");
				adminAccess();
				break;
			}
			break;
		case 2:
			System.out.println("Calculating bill for activated customers");
			// PostPaidBillGenerator.billGeneration();
			break;
		case 3:
			System.out.println("logout successfull");
			System.exit(0);

		default:
			System.out.println("wrong input entered ");
			adminAccess();
		}
	}
}
