package com.MobileBillGenerator.plans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.MobileBillGenerator.billPlans.PostpaidBillGeneration;

/**
 * this class is used for postpaid bill generation
 * 
 * @author RANJHS
 *
 */
public class PostPaidBillGenerator {

	static final String fileName1 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGeneratorr\\src\\postpaidplanactivateduserds4.txt";
	static List<PostpaidBillGeneration> list = new ArrayList<PostpaidBillGeneration>();
	
	/**
	 * this method is used for generating the bill according to the offer
	 */

	@SuppressWarnings("unchecked")
	public static void billGeneration() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int count = 0;
	
		FileInputStream fis;
		try {
			fis = new FileInputStream(fileName1);
			
			ObjectInputStream ois = new ObjectInputStream(fis);
			list = (List<PostpaidBillGeneration>) ois.readObject();

			list.get(0).setBalance(50);
			list.get(1).setBalance(75);
			list.get(2).setBalance(75);
			list=PostpaidBillGeneration.getRegister();
		;
			String mobile = sc.next();
			for (PostpaidBillGeneration bill : list) {
				if (mobile.equals(bill.getMobilenumber())) {
					count = 1;
					if (bill.getActivation() == 1) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						Calendar c = Calendar.getInstance();
						c.setTime(new Date());
						c.add(Calendar.DATE, 24);
						String date1 = sdf.format(c.getTime());

						if (date1.compareTo(bill.getDate()) == 0) {
							System.out.println("todays date:" + bill.getDate());
							int balance = bill.getBalance();
							if (bill.getOffername1() == 399) {
								if (balance > 40) {

									balance = balance - 40;
									balance = balance * 10;
									double postPaidBill = 399 + balance;
									System.out.println("Your Actual Balance is Rs.399");
									System.out.println(
											"The Bill is Rs." + postPaidBill + " As you've consumed more balance");
									
								} else if (balance < 40) {
									balance = 40 - balance;
									double newbal = balance;
									double postPaidBill = 399;
									System.out.println("The Bill is Rs." + postPaidBill + " the balance- " + newbal + "GB you've not consumed will be added to next month");
									
								} else if (balance == 40) {
									System.out.println("The Bill is Rs.399");
									
								}
								
							}
							if (bill.getOffername1() == 499) {
								if (balance > 75) {
									balance = balance - 75;
									balance = balance * 10;
									double postPaidBill = 499 + balance;
									System.out.println("Your Actual Balance is Rs.499");
									System.out.println(
											"The Bill is Rs." + postPaidBill + " As you've consumed more balance");
									
								} else if (balance < 75) {
									balance = 75 - balance;
									double newbal = balance;
									double postPaidBill = 499;
									System.out.println("The Bill is Rs." + postPaidBill + " the balance- " + newbal + "GB you've not consumed will be added to next month");
								
								} else if (balance == 75) {
									System.out.println("The Bill is Rs.499");
								
								}
							}
							if (bill.getOffername1() == 599) {
								if (balance > 90) {
									balance = balance - 90;
									balance = balance * 10;
									double postPaidBill = 599 + balance;
									System.out.println("Your Actual Balance is Rs.599");
									System.out.println(
											"The Bill is Rs." + postPaidBill + " As you've consumed more balance");
									
									
								} else if (balance < 90) {
									balance = 90 - balance;
									double newbal = balance;
									double postPaidBill = 599;
									System.out.println("The Bill is Rs." + postPaidBill + " the balance- " + newbal + "GB you've not consumed will be added to next month");
									
								} else if (balance == 90) {
									System.out.println("The Bill is Rs.599");
								}
							}
							if (bill.getOffername1() == 749) {
								if (balance > 120) {
									balance = balance - 120;
									balance = balance * 10;
									double postPaidBill = 749 + balance;
									System.out.println("Your Actual Balance is Rs.749");
									System.out.println(
											"The Bill is Rs." + postPaidBill + " As you've consumed more balance");
								} else if (balance < 120) {
									balance = 120 - balance;
									double newbal = balance;
									double postPaidBill = 749;
									System.out.println("The Bill is Rs." + postPaidBill + " the balance- " + newbal
											+ " you've not consumed will be added to next month");
								} else if (balance == 120) {
									System.out.println("The Bill is Rs.749");
								}
							}
						} else if (date1.compareTo(bill.getDate()) > 0) {

							SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");

							String date = s.format(bill.getDate());
							try {
								System.out.println("Pay ur Bill on or Before " + s.parse(date));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							System.out.println("Your pack is not Expired");
						}
					}
				}

			}
			if (count == 0) {
				System.out.println("Number Not Found");
				System.exit(0);
			}
			ois.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		
	}

}
