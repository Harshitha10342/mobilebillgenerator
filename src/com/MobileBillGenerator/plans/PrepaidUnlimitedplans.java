package com.MobileBillGenerator.plans;



import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * This class is used for adding, updating, displaying the unlimited packs
 * 
 * @author RANJHS
 *
 */


public class PrepaidUnlimitedplans implements Serializable {

	/**
	 * This is a serializable ID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * This class contains all parameters of product list like
	 * productname,price,quantity Also have the addproduct method ,and display
	 * method , delete product
	 */

	

		
		



		private int offerName;
		private String offerDescription;
		private int validity;
		
		PrepaidUnlimitedplans(int offerName,String offerDescription,int validity)
		{
			this.offerName=offerName;
			this.offerDescription=offerDescription;
			this.validity=validity;
			
		}
		PrepaidUnlimitedplans()
		  {
			  
		  }
		
		
		/**
		 * @return the offerName
		 */
		public int getOfferName() {
			return offerName;
		}

		

		/**
		 * @return the offerDescription
		 */
		public String getOfferDescription() {
			return offerDescription;
		}

		

		/**
		 * @return the offerPacks
		 */
		public int getValidity() {
			return validity;
		}

		

		 
		// To create the object reference to the map class
			static  Map<Integer, PrepaidUnlimitedplans> mapobject = new HashMap<Integer, PrepaidUnlimitedplans>();
			static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\unlimitedplans.txt";
			/**
			 * This method is used to add the products to the file
			 * 
			 * @param sc
			 * @param number
			 */
			public static void addUnlimitedPlans() {
				// It takes the number to insert in the file
				//System.out.println(mapobject);
				@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);
				System.out.println("do u want to add new offers to the network....if Yes enter NUMBER...else enter 0");
				System.out.println("Enter the number How many offers you will add :");
				int number = sc.nextInt();
				int keyvalue;
				int offerName;
				String offerDescription;
				int validity;
				try {
					File f=new File(fileName);
					if(f.length()==0) {
					FileOutputStream fileOut = new FileOutputStream(fileName);
					@SuppressWarnings("resource")
					ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
					objOut.writeObject(mapobject);
					}
					// Reading the object from a file
					FileInputStream file = new FileInputStream(fileName);
					@SuppressWarnings("resource")
					ObjectInputStream in = new ObjectInputStream(file);
					// Method for deserialization of object
					//@SuppressWarnings("unchecked")
					@SuppressWarnings("unchecked")
					Map<Integer, PrepaidUnlimitedplans> mapobject =(Map<Integer, PrepaidUnlimitedplans>)in.readObject();
					System.out.println("The last key value of the map is :"+mapobject.size());
					keyvalue=mapobject.size();
					// This condition used to iterate the loop based on number of products add to
					// the list
					for (int i = 1; i <= number; i++) {
						keyvalue = keyvalue+1;
						System.out.println("the key value is : " +keyvalue);
						
						System.out.println("Enter the Offer name :");
						offerName = sc.nextInt();	
						sc.nextLine();
						System.out.println("Enter the  Offer description :");
						offerDescription = sc.nextLine();
						
						System.out.println("Enter the Offer Validity:");
						validity = sc.nextInt();
						// This condition is used to add product object to map
						mapobject.put(keyvalue, new PrepaidUnlimitedplans(offerName, offerDescription, validity));
						// This statements are used get the data from a particular File
						FileOutputStream file1 = new FileOutputStream(f);
						ObjectOutputStream out = new ObjectOutputStream(file1);
						// This statements are used to add the product to File
						out.writeObject(mapobject);
						// ObjectOutputStream close
						out.close();
						// File close
						file1.close();
						System.out.println("Plans are Added in the File ");
					}

				} catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("IOException is Exception");
				} catch (Exception ex) {
					ex.getMessage();
					System.out.println("You Enter the wrong input");
				}
				
			}

		/**
		 * This method is used to display the product what products are available in
		 * file
		 * 
		 * @throws ClassNotFoundException
		 */
		//@SuppressWarnings("unchecked")
		public static int displayUnlimitedPlans(int search) {
			int date=0;
			try {

				// Reading the object from a file
				//Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
				FileInputStream file = new FileInputStream(fileName);
				ObjectInputStream input = new ObjectInputStream(file);
				// Method for deserialization of object
		
				@SuppressWarnings("unchecked")
				Map<Integer, PrepaidUnlimitedplans> mapobject = (Map<Integer, PrepaidUnlimitedplans>)input.readObject();
				for (int key : mapobject.keySet()) {
					if(search==key) {
					PrepaidUnlimitedplans product = mapobject.get(key);
					System.out.print("recharge done with RC " + product.getOfferName());
					System.out.print(" and got benifits : " + product.getOfferDescription());
					System.out.print(" with Validity: " + product.getValidity() +"days");
					System.out.println();
					date=product.getValidity();
				}
				}
				// ObjectInputStream close
				input.close();
				// File close
				file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				System.out.println("IOException is Exception");
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
				System.out.println("ClassNotFoundException");
			}
			return date;
		}
		public static  void displayUnlimited() {
			int count=1;
			try {

				// Reading the object from a file
				//Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
				FileInputStream file = new FileInputStream(fileName);
				ObjectInputStream input = new ObjectInputStream(file);
				// Method for deserialization of object
		
				@SuppressWarnings("unchecked")
				Map<Integer, PrepaidUnlimitedplans> mapobject = (Map<Integer, PrepaidUnlimitedplans>)input.readObject();
				for (int key : mapobject.keySet()) {
					
					PrepaidUnlimitedplans product = mapobject.get(key);
					System.out.print(count+" recharge with  " + product.getOfferName());
					System.out.print(" and get : " + product.getOfferDescription());
					System.out.print("with Validity: " + product.getValidity() +"days");
					System.out.println();
					count++;
				}
				
				// ObjectInputStream close
				input.close();
				// File close
				file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				System.out.println("IOException is Exception");
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
				System.out.println("ClassNotFoundException");
			}

		}
		public static  void updateUnlimitedPlans() {
			@SuppressWarnings("resource")
			Scanner sc=new Scanner(System.in);
			System.out.println("Select choose to update:");
			displayUnlimited();		
			int updatepostpaidkeyUnlimitedplans = sc.nextInt();
			try {

				// Reading the object from a file
				//Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
				FileInputStream file = new FileInputStream(fileName);
				ObjectInputStream input = new ObjectInputStream(file);
				// Method for deserialization of object
		
				@SuppressWarnings("unchecked")
				Map<Integer, PrepaidUnlimitedplans> mapobject = (Map<Integer, PrepaidUnlimitedplans>)input.readObject();
				for (int key : mapobject.keySet()) {
					if(updatepostpaidkeyUnlimitedplans==key) {
					PrepaidUnlimitedplans product = mapobject.get(key);
					System.out.print("recharge done with RC " + product.getOfferName());
					System.out.print(" and got benifits : " + product.getOfferDescription());
					System.out.print(" with Validity: " + product.getValidity() +"days");
					System.out.println();
					System.out.println("Enter the Offer name :");
					int offerName = sc.nextInt();	
					sc.nextLine();
					System.out.println("Enter the  Offer description :");
					String offerDescription = sc.nextLine();
					
					System.out.println("Enter the Offer Validity:");
					int validity = sc.nextInt();
					// This condition is used to add product object to map
					mapobject.put(updatepostpaidkeyUnlimitedplans, new PrepaidUnlimitedplans(offerName, offerDescription, validity));
					// This statements are used get the data from a particular File
					FileOutputStream file1 = new FileOutputStream(fileName);
					ObjectOutputStream out = new ObjectOutputStream(file1);
					// This statements are used to add the product to File
					out.writeObject(mapobject);
					// ObjectOutputStream close
					out.close();
					// File close
					file1.close();
					System.out.println("Plans are Updated successfully ");
					
				}
				}
				
				// ObjectInputStream close
				input.close();
				// File close
				file.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				System.out.println("IOException is Exception");
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
				System.out.println("ClassNotFoundException");
			}
 
		}
	}
