package com.MobileBillGenerator.billPlans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.MobileBillGenerator.plans.PostpaidPacks;


public class PostpaidBillGeneration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static  List<PostpaidBillGeneration> list = new ArrayList<PostpaidBillGeneration>();
	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\postpaidplans.txt";
	static final String fileName1 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\postpaidplanactivateduserds4.txt";
	private String mobilenumber;
	private  int offername1;
	private int balance;
	private String expiryDate;
	private int activation;
	
	public int getActivation() {
		return activation;
	}
	public void setActivation(int activation) {
		this.activation = activation;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public  int getOffername1() {
		return offername1;
	}
	public  void setOffername1(int offername1) {
		this.offername1 = offername1;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public String getDate() {
		return expiryDate;
	}
	public void setDate(String date) {
		this.expiryDate = date;
	}
	@Override
	public String toString() {
		return "[mobilenumber=" + mobilenumber + ", offername1=" + offername1 + ", balance="
				+ balance + ", date=" + expiryDate + ", activation=" + activation + "]";
	}
	public static void  bill(int packoption,String mobilenumber,int activation,String date) {
		
		
		try {

			// Reading the object from a file
			//Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream input = new ObjectInputStream(file);
			// Method for deserialization of object
	
			@SuppressWarnings("unchecked")
			Map<Integer, PostpaidPacks> mapobject = (Map<Integer, PostpaidPacks>)input.readObject();
			for (int key : mapobject.keySet()) {
				if(packoption==key) {
					
					PostpaidPacks product = mapobject.get(key);
					int offername=product.getOfferName();
				
				try {
					File f=new File(fileName1);
					if(f.length()==0) {
					FileOutputStream fileOut = new FileOutputStream(fileName1);
					@SuppressWarnings("resource")
					ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
					objOut.writeObject(list);
					}
					list=getRegister();
					PostpaidBillGeneration newUser = new PostpaidBillGeneration();
					newUser.setMobilenumber(mobilenumber);
					newUser.setOffername1(offername);
					//newUser.setBalance(0);
					newUser.setDate(date);
					newUser.setActivation(activation);
					list.add(newUser);

					setRegister(list);
					list = getRegister();
					
					
//					
					} catch (IOException ex) {
						ex.printStackTrace();
						System.out.println("IOException is Exception");
					} catch (Exception ex) {
						ex.getMessage();
						System.out.println("You Enter the wrong input");
					}
				}	
			}
			
			// ObjectInputStream close
			input.close();
			// File close
			//file.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("IOException is Exception");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("ClassNotFoundException");
		}
		
	}
	 
	
		public static void setRegister(List<PostpaidBillGeneration> list) {
			FileOutputStream file = null;
			ObjectOutputStream out = null;
			try {

				for (int i = 0; i < list.size(); i++) {
					file = new FileOutputStream(fileName1);
					out = new ObjectOutputStream(file);
					out.writeObject(list);
				}
				// close all the opened resources
				out.close();
				file.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		@SuppressWarnings("unchecked")

		/**
		 * this method is used to read/retrieve the data from the file
		 * 
		 * @return it returns the user data which is stored in the list
		 */
		public static List<PostpaidBillGeneration> getRegister() {
			List<PostpaidBillGeneration> list2 = new ArrayList<PostpaidBillGeneration>();
			try {
				// Saving of object in a file
				FileInputStream file = new FileInputStream(fileName1);
				ObjectInputStream in = new ObjectInputStream(file);
				// read the data from the list
				list2 = (List<PostpaidBillGeneration>) in.readObject();
				// close all the opened resources
				in.close();
				file.close();
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.println("IOException is caught");
			}
			return list2;

		}


	
}
