package com.MobileBillGenerator.billPlans;

import java.text.SimpleDateFormat;
import java.util.*;

import com.MobileBillGenerator.display.DateDisplay;
import com.MobileBillGenerator.login.LoginUtilities;
import com.MobileBillGenerator.plans.PostpaidPacks;



/**
 * @author RANJHS This class is used to display the PostPaid plans
 */

public class PostPaidBillPlans {
	static int activated=0;
	//static List<String> postlist=new ArrayList<String>();
	static final String fileName1 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\postpaidplanactivateduserds4.txt";
	static int count=0;
	static  List<PostpaidBillGeneration> list = new ArrayList<PostpaidBillGeneration>();
	
	/**
	 * this method calls the postPaidinput to call  all the different PostPaid plans to the
	 * registered users
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 */
	public static void postpaidPlans()  {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		list=PostpaidBillGeneration.getRegister();
		 String amount;
		 int index=-1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		//c.add(Calendar.DATE, 24);
		String date1 = sdf.format(c.getTime());
		Iterator<PostpaidBillGeneration> i= list.iterator();
		
		
		while(i.hasNext()) {
			index=index+1;
			PostpaidBillGeneration user=(PostpaidBillGeneration) i.next();
			
			
			
			 if(user.getMobilenumber().equalsIgnoreCase(LoginUtilities.mobileNumberInput) && (user.getDate().equals(date1) ||date1.compareTo(user.getDate())<0)) {
				 amount=UserPostPaidBill.billGeneration(user.getMobilenumber());
				 System.out.println(amount);
				 System.out.println("1.pay the bill ammount");
				 System.out.println("2.Exit ");
				 int choice=0;
					try {
						choice= sc.nextInt();
					}catch(Exception e) {
						System.out.println("the input is of not integer");
						System.exit(0);
					}
					if (choice==1) {
					System.out.println("1.Proceed to Recharge");
					int confirm = 0;
					try {
						confirm = sc.nextInt();
					} catch (Exception e) {
						System.out.println("the input is of not integer");
						System.exit(0);
					}
					
					if(confirm==1) {
				
						System.out.println("YOUR HAVE PAID THE BILL SUCCESSFULLY");
						System.out.println("continue to activate your plan");
						activated=0;
						list.remove(index);
						PostpaidBillGeneration.setRegister(list);
						list=PostpaidBillGeneration.getRegister();
						System.out.println(list);
					
						postPaidInput( );
						
						break;
					}
					} else if(choice==2) {
						
						System.out.println("PAY " + amount +" in order to use our services");
						System.exit(0);
					}
					
				 
				 
				
			}
			 else if(user.getMobilenumber().equalsIgnoreCase(LoginUtilities.mobileNumberInput) && user.getActivation() == 1 && date1.compareTo(user.getDate())>0 ) {
				System.out.println("your plan is activate with amount plan "+user.getOffername1());
				System.out.println("your plan will expires on "+user.getDate());
				System.exit(0);
			}
		
		}
		System.out.println("--------------");
		System.out.println("POSTPAID PLANS");
		System.out.println("--------------");
		postPaidInput();
		
	}
		
/**
* this method calls the packgenerator method  to display all the different PostPaid plans to the
* registered users
* * @param sc this is used to read the input(plans) from the user
*/		
		
		
		
		
	public  static void postPaidInput() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Select your Plan:");
		PostpaidPacks.displayPostpaid();
		System.out.println("0.EXIT ");
		int postPaidPack=0;
		try {
			postPaidPack= sc.nextInt();
		}catch(Exception e) {
			System.out.println("the input is of not integer");
			
			
		}
		switch (postPaidPack) {
		case 1:
			System.out.println("1.Activate Your Plan");
			System.out.println("0.Back To Main Menu");
			int packOption=0;
			try {
				packOption= sc.nextInt();
			}catch(Exception e) {
			System.out.println("the input is of not integer");
			
			}
			switch(packOption) {
				
			case 1: System.out.println("ARE YOU SURE TO DO RECHARGE");
			System.out.println("1.recharge");
			System.out.println("0.back");
			int confirm = 0;
			try {
				confirm = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			
			if(confirm==1) {
				System.out.println("YOUR PACK IS ACTIVATED");
				activated=1;
				int validDate=PostpaidPacks.displayPostpaidPlans(postPaidPack);
				DateDisplay.postpaidExpirayDate(validDate);
				PostpaidBillGeneration.bill(postPaidPack,LoginUtilities.mobileNumberInput,activated,DateDisplay.date);
				
				break;
				
			} else {
				postPaidInput();
				
			
			}
			break;
				
			case 0:count=count+1;
				if(count!=5)
				{
				postPaidInput();
				} else {
					System.out.println("Too many checks,please try after some time");
					System.exit(0);
				}
		
			default :
				count=count+1;
				System.out.println("Wrong input entered,please enter again");
				postPaidInput();
				break;
			}
			break;
		case 2:
			
			System.out.println("1.Activate Your Plan");
			System.out.println("0.Back To Main Menu");
			int packOption1=0;
			try {
				packOption1= sc.nextInt();
			}catch(Exception e) {
			System.out.println("the input is of not integer");
			
			}
			switch(packOption1) {
				
			case 1: System.out.println("ARE YOU SURE TO DO RECHARGE");
						
			System.out.println("1.recharge");
			System.out.println("0.back");
			int confirm = 0;
			try {
				confirm = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			
			if(confirm==1) {
				activated=1;
				System.out.println("YOUR PACK IS ACTIVATED");
				int validDate=PostpaidPacks.displayPostpaidPlans(postPaidPack);
				DateDisplay.postpaidExpirayDate(validDate);
				PostpaidBillGeneration.bill(postPaidPack,LoginUtilities.mobileNumberInput,activated,DateDisplay.date);
				break;
				
			} else {
				postPaidInput();
			
			}
			break;
				
			case 0:count=count+1;
				if(count!=5)
				{
					postPaidInput();
				} else {
					System.out.println("Too many checks,please try after some time");
					System.exit(0);
				}
				
			default :
				count=count+1;
				System.out.println("wrong input entered,please enter again");
				postPaidInput();
				break;
			}
			break;
		case 3:
		
			System.out.println("1.Activate Your Plan");
			System.out.println("0.Back To Main Menu");
			int packOption2=0;
			try {
				packOption2= sc.nextInt();
			}catch(Exception e) {
			System.out.println("the input is of not integer");
			
			}
			switch(packOption2) {
				
			case 1: System.out.println("ARE YOU SURE TO DO RECHARGE");
			System.out.println("1.recharge");
			System.out.println("0.back");
			int confirm = 0;
			try {
				confirm = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
			
			}
			
			if(confirm==1) {
				activated=1;
				System.out.println("YOUR PACK IS ACTIVATED");
				int validDate=PostpaidPacks.displayPostpaidPlans(postPaidPack);
				DateDisplay.postpaidExpirayDate(validDate);
				PostpaidBillGeneration.bill(postPaidPack,LoginUtilities.mobileNumberInput,activated,DateDisplay.date);
				
				break;
				
			} else {
				postPaidInput();
			
			}
			break;
				
			case 0:count=count+1;
				if(count!=5)
				{
					postPaidInput();
				} else {
					System.out.println("Too many checks,please try after some time");
					System.exit(0);
				}
			default :
				count=count+1;
				System.out.println("wrong input entered,please enter again");
				postPaidInput();
				break;
			}
			break;
		case 4:
			
			System.out.println("1.Activate Your Plan");
			System.out.println("0.Back To Main Menu");
			int packOption4=0;
			try {
				packOption4= sc.nextInt();
			}catch(Exception e) {
			System.out.println("the input is of not integer");
			
			}
			switch(packOption4) {
				
			case 1: System.out.println("ARE YOU SURE TO DO RECHARGE");
			System.out.println("1.recharge");
			System.out.println("0.back");
			int confirm = 0;
			
			try {
				confirm = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			
			if(confirm==1) {
				System.out.println("YOUR PACK IS ACTIVATED");
				activated=1;
				int validDate=PostpaidPacks.displayPostpaidPlans(postPaidPack);
				DateDisplay.postpaidExpirayDate(validDate);
				PostpaidBillGeneration.bill(postPaidPack,LoginUtilities.mobileNumberInput,activated,DateDisplay.date);	
				break;
				
			} else {
				postPaidInput();
			
			}
						break;
				
			case 0:count=count+1;
				if(count!=5)
				{
					postPaidInput();
				} else {
					System.out.println("Too many checks,please try after some time");
					System.exit(0);
				}
		
			default :
				count=count+1;
				System.out.println("wrong input entered,please enter again");
				postPaidInput();
				break;
			}
			break;
		case 0:System.out.println("programe is stopped");
				System.exit(0);
		default:
			System.out.println("enter the correct option");
			postPaidInput();
			break;
		}

	}




}

