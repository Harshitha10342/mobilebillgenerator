package com.MobileBillGenerator.billPlans;


import java.util.Scanner;

import com.MobileBillGenerator.display.DateDisplay;
import com.MobileBillGenerator.plans.PrepaidDatapacks;
import com.MobileBillGenerator.plans.PrepaidSpecialpacks;
import com.MobileBillGenerator.plans.PrepaidTalktimePlans;
import com.MobileBillGenerator.plans.PrepaidUnlimitedplans;

/**
 * This class is used to display the prepaid plans
 * 
 * @author RANJHS
 *
 */

public class PrePaidBillPlans {


	static int count = 0;
	Scanner sc = new Scanner(System.in);
	/**
	 * this method is uses input method to call all the different PrePaid plans to
	 * the registered users
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 * @throws FileNotFoundException it occurs when file is not found
	 */
	public static void prepaidPlans() {
		System.out.println("--------");
		System.out.println("PREPAID PLANS That ARE ELIGIBLE FOR YOUR MOBILE NUMBER");
		System.out.println("--------");
		// users select their pack
		inputPlans();
		
	}

	/**
	 * this method gives the choices to user to select the different PrePaid plans
	 * available to the registered number
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 */

	public static void inputPlans() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Select your Plan:");
		System.out.println("1.Unlimited Packs");
		System.out.println("2.Talk Time Packs");
		System.out.println("3.Data Packs");
		System.out.println("4.Special Offers");
		int pack = 0;
		try {

			pack = sc.nextInt();
			// System.out.println("dfad");
		} catch (Exception e) {
			System.out.print("the input is of not integer");
			
			//System.exit(0);

		}
		SwitchPack: switch (pack) {
		case 1:
			// it will show the unlimited plan offers for the user registerd mobilenumber
			unlimitedPack();
			break SwitchPack;

		case 2:
			// it will show the talktime offers available for the user registerd
			// mobilenumber
			talkTimePlans();
			break SwitchPack;
		case 3:
			// it will show the datapack offers avvaialble for the user registerd
			// mobilenumber
			dataPacks();
			break SwitchPack;
		case 4:
			// it will show the specialoffers available for the user registerd mobilenumber
			specialRecharge();
			break SwitchPack;

		default:
			System.out.println("enter the correct input");
			inputPlans();
			
		}

	}

	/**
	 * this method is uses packGenerator method to dispaly all the talktimeplans to
	 * the registered user
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 */

	public static void talkTimePlans() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose your Pack:");
		PrepaidTalktimePlans.displayTalktimePlans();
		System.out.println("0.Back To Main Menu");

		int talktimePackOption = 0;
		try {
			talktimePackOption = sc.nextInt();
		} catch (Exception e) {
			System.out.print("the input is of not integer");
		
		}
		// users selects their talktime plan
		switch (talktimePackOption) {
		case 1:
			
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int talktimeSelection = 0;
			try {
				talktimeSelection = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (talktimeSelection) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					PrepaidTalktimePlans.displayTalktimePlans(talktimePackOption);
					
					
					break;
					
				} else {
					talkTimePlans();
				
				}
			
				break;

			case 0:
				count = count + 1;
				talkTimePlans();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				talkTimePlans();
				
			}
			break;

		case 2:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int talktimeSelection1 = 0;
			try {
				talktimeSelection1 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (talktimeSelection1) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					PrepaidTalktimePlans.displayTalktimePlans(talktimePackOption);
					
					
					break;
					
				} else {
					talkTimePlans();
				
				}
				break;
			case 0:
				count = count + 1;
				talkTimePlans();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				talkTimePlans();
			}
			break;
		case 3:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int talktimeSelection2 = 0;
			try {
				talktimeSelection2 = sc.nextInt();
			} catch (Exception e) {
				System.out.print("the input is of not integer");
				
			}
			switch (talktimeSelection2) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					PrepaidTalktimePlans.displayTalktimePlans(talktimePackOption);
					
					break;
					
				} else {
					talkTimePlans();
				
				}
				break;
			case 0:
				count = count + 1;
				talkTimePlans();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				talkTimePlans();
			}
			break;

		case 0:
			count = count + 1;
			if (count != 5) {
				inputPlans();
			} else {
				System.out.println("Too many checks,please try after some time");
				System.exit(0);
			}
			break;

		default:
			System.out.println("wrong input entered,please enter again");
			talkTimePlans();

		}
	}

	/**
	 * this method is uses packGenerator method to dispaly all the unlimitedplans to
	 * the registered user
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 */
	public static void unlimitedPack() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		System.out.println("Choose your Pack:");
		PrepaidUnlimitedplans.displayUnlimited(); 
		System.out.println("0.Back To Main Menu");
		int unlimitedPackOption = 0;
		try {
			unlimitedPackOption = sc.nextInt();
		} catch (Exception e) {
			System.out.println("the input is of not integer");
			
		}
		// users selects their unlimited plan
		switch (unlimitedPackOption) {
		case 1:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int unlimitedPackSelection = 0;
			try {
				unlimitedPackSelection = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
			
			}
			switch (unlimitedPackSelection) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidUnlimitedplans.displayUnlimitedPlans(unlimitedPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					break;
					
				} else {
					unlimitedPack();
				
				}
				break;
			case 0:
				count = count + 1;
				unlimitedPack();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				unlimitedPack();
			}
			break;
		case 2:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int unlimitedPackSelection1 = 0;
			try {
				unlimitedPackSelection1 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (unlimitedPackSelection1) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidUnlimitedplans.displayUnlimitedPlans(unlimitedPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					break;
					
				} else {
					unlimitedPack();
				
				}
				break;
			
			case 0:
				count = count + 1;
				unlimitedPack();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;
			default:
				System.out.println("wrong input entered,please enter again");
				unlimitedPack();
			}
			break;
		case 3:
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");

			int unlimitedPackSelection2 = 0;
			try {
				unlimitedPackSelection2 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (unlimitedPackSelection2) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
				
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidUnlimitedplans.displayUnlimitedPlans(unlimitedPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					unlimitedPack();
				
				}
				break;
			case 0:
				count = count + 1;
				unlimitedPack();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				unlimitedPack();
			}
			break;

		case 0:
			count = count + 1;
			if (count != 5) {
				inputPlans();
			} else {
				System.out.println("Too many checks,please try after some time");
				System.exit(0);
			}
			break;

		default:
			System.out.println("wrong input entered,please enter again");
			unlimitedPack();

		}
	}

	/**
	 * this method uses packGenerator method to dispaly all the dataplans available
	 * to the registered user
	 * 
	 * @param sc this is used to read the input(plans) from the user
	 */

	public static void dataPacks() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose your Pack:");
		PrepaidDatapacks.displayDatapack();
		System.out.println("0.Back To Main Menu");
		int dataPackOption = 0;
		try {
			dataPackOption = sc.nextInt();
		} catch (Exception e) {
			System.out.println("the input is of not integer");
			
		}
		// users selects their data pack
		switch (dataPackOption) {
		case 1:
			
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int dataPackSelection = 0;
			try {
				dataPackSelection = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (dataPackSelection) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidDatapacks.displayDatapackPlans(dataPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					break;
					
				} else {
					dataPacks();
				
				}
				break;
			case 0:
				count = count + 1;
				dataPacks();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				dataPacks();
			}
			break;
		case 2:
			
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int dataPackSelection1 = 0;
			try {
				dataPackSelection1 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
			
			}
			switch (dataPackSelection1) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidDatapacks.displayDatapackPlans(dataPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					dataPacks();
				
				}
			case 0:
				count = count + 1;
				dataPacks();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				dataPacks();

			}
			break;

		case 3:
			
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");

			int dataPackSelection2 = 0;
			try {
				dataPackSelection2 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
			
			}
			switch (dataPackSelection2) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidDatapacks.displayDatapackPlans(dataPackOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					dataPacks();
				
				}
				break;

			case 0:
				count = count + 1;
				dataPacks();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				dataPacks();

			}
			break;

		case 0:
			count = count + 1;
			if (count != 5) {
				inputPlans();
			} else {
				System.out.println("Too many checks,please try after some time");
				System.exit(0);
			}
			break;

		default:
			System.out.println("wrong input entered,please enter again");
			dataPacks();

		}
	}

	/**
	 * this method is uses packGenerator method to dispaly all the specialrecharge
	 * offers aailable to the registered user
	 * @param sc this is used to read the input(plans) from the user
	 */

	public static void specialRecharge() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose your Pack:");
		PrepaidSpecialpacks.displaySpecialpack();
		System.out.println("0.Back To Main Menu");
		int specialRechargeOption = 0;
		try {
			specialRechargeOption = sc.nextInt();
		} catch (Exception e) {
			System.out.println("the input is of not integer");
		
		}
		// users selects their special offer
		switch (specialRechargeOption) {
		case 1:
			
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");

			int specialSelection = 0;
			try {
				specialSelection = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (specialSelection) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidSpecialpacks.displaySpecialpackPlans(specialRechargeOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					specialRecharge();
				
				}
				break;
			case 0:
				count = count + 1;
				specialRecharge();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				specialRecharge();

			}
			break;

		case 2:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");

			int specialSelection1 = 0;
			try {
				specialSelection1 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (specialSelection1) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidSpecialpacks.displaySpecialpackPlans(specialRechargeOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					specialRecharge();
				
				}
				break;
			case 0:
				count = count + 1;
				specialRecharge();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				specialRecharge();

			}
			break;
		case 3:
		
			System.out.println("1.Proceed To Recharge");
			System.out.println("0.Back To Previous Menu");
			System.out.println("2.Main Menu");
			int specialSelection2 = 0;
			try {
				specialSelection2 = sc.nextInt();
			} catch (Exception e) {
				System.out.println("the input is of not integer");
				
			}
			switch (specialSelection2) {

			case 1:
				System.out.println("ARE YOU SURE TO DO RECHARGE");
				System.out.println("1.recharge");
				System.out.println("0.back");
				int confirm = 0;
				try {
					confirm = sc.nextInt();
				} catch (Exception e) {
					System.out.println("the input is of not integer");
					
				}
				
				if(confirm==1) {
					System.out.println("RECHARGE SUCCESFFULL");
					int validDate=PrepaidSpecialpacks.displaySpecialpackPlans(specialRechargeOption);
					DateDisplay.prepaidExpirayDate(validDate);
					
					
					break;
					
				} else {
					specialRecharge();
				
				}
			case 0:
				count = count + 1;
				specialRecharge();
				break;

			case 2:
				count = count + 1;
				inputPlans();
				break;

			default:
				System.out.println("wrong input entered,please enter again");
				specialRecharge();
			}
			break;

		case 0:
			count = count + 1;
			if (count != 5) {
				inputPlans();
			} else {
				System.out.println("Too many checks,please try after some time");
				System.exit(0);
			}
			break;

		default:
			System.out.println("wrong input entered,please enter again");
			specialRecharge();

		}

	}

	
}
