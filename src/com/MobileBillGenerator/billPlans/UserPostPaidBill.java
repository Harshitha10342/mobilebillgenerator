package com.MobileBillGenerator.billPlans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.MobileBillGenerator.billPlans.PostpaidBillGeneration;

/**
 * this class is used for postpaid bill generation
 * 
 * @author RANJHS
 *
 */
public class UserPostPaidBill {

	static final String fileName1 = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGeneratorr\\src\\postpaidplanactivateduserds4.txt";
	static List<PostpaidBillGeneration> list = new ArrayList<PostpaidBillGeneration>();
	static String postPaid;

	/**
	 * this method is used for generating the bill according to the offer
	 */

	@SuppressWarnings({ "unchecked", "unused" })
	public static String billGeneration(String mobile) {
	
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		FileInputStream fis;
		try {
			fis = new FileInputStream(fileName1);

			@SuppressWarnings("resource")
			ObjectInputStream ois = new ObjectInputStream(fis);
			list = (List<PostpaidBillGeneration>) ois.readObject();
			list = PostpaidBillGeneration.getRegister();
			for (PostpaidBillGeneration bill : list) {

				if (mobile.equals(bill.getMobilenumber())) {
					if (bill.getActivation() == 1) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						Calendar c = Calendar.getInstance();
						c.setTime(new Date());
						c.add(Calendar.DATE, 24);
						String date1 = sdf.format(c.getTime());

						if (date1.compareTo(bill.getDate()) == 0) {
							System.out.println("your plan expired on:" + bill.getDate());
							if (bill.getOffername1() == 399) {
								postPaid = "The Postpaid Bill is Rs.399";
								return postPaid;
							}

						}
						if (bill.getOffername1() == 499) {

							postPaid = "The Postpaid Bill is Rs.499";
							return postPaid;
						}
					}
					if (bill.getOffername1() == 599) {

						postPaid = "The Postpaid Bill is Rs.599";
						return postPaid;
					}
				}
				if (bill.getOffername1() == 749) {

					postPaid = "The Postpaid Bill is Rs.749";
					return postPaid;
				}
			}
			

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		return postPaid;

	}

}