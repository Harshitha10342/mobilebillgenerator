package com.mobilebillgeneration.plans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class is used for adding, updating, displaying the postpaid packs
 * 
 * @author RANJHS
 *
 */
public class PostpaidPacks implements Serializable {

	/**
	 * This is a serializable ID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * This class contains all parameters of product like list
	 * productname,price,quantity and also have the addproduct method, display
	 * method, delete product method.
	 */

	private int offerName;
	private String offerDescription;
	private int validity;
	static Scanner sc = new Scanner(System.in);

	PostpaidPacks(int offerName, String offerDescription, int validity) {
		this.offerName = offerName;
		this.offerDescription = offerDescription;
		this.validity = validity;

	}
	PostpaidPacks() 
	{
		
	}

	/**
	 * @return the offerName
	 */
	public int getOfferName() {
		return offerName;
	}

	/**
	 * @return the offerDescription
	 */
	public String getOfferDescription() {
		return offerDescription;
	}

	/**
	 * @return the offerPacks
	 */
	public int getValidity() {
		return validity;
	}

	// To create the object reference to the map class
	static Map<Integer, PostpaidPacks> mapobject = new HashMap<Integer, PostpaidPacks>();
	static final String fileName = "C:\\Users\\IMVIZAG\\eclipse-workspace\\MobileBillGenerator\\src\\postpaidplans.txt";

	/**
	 * This method is used to add the postpaid offers to the file
	 * 
	 * @param number used for updating the offers
	 */
	public static void addPostpaidPlans() {
		// It takes the number to insert in the file
		// System.out.println(mapobject);
		System.out.println("Do u want to add new offers to the network. 1)If Yes enter NUMBER \n 2)else enter 0");
		System.out.println("How many offers you want to add :");
		int number = sc.nextInt();
		int keyvalue;
		int offerName;
		String offerDescription;
		int validity;
		try {
			File f = new File(fileName);
			if (f.length() == 0) {
				FileOutputStream fileOut = new FileOutputStream(fileName);
				@SuppressWarnings("resource")
				ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
				objOut.writeObject(mapobject);
			}
			// Reading the object from a file
			FileInputStream file = new FileInputStream(fileName);
			@SuppressWarnings("resource")
			ObjectInputStream in = new ObjectInputStream(file);
			// Method for deserialization of object
			@SuppressWarnings("unchecked")
			Map<Integer, PostpaidPacks> mapobject = (Map<Integer, PostpaidPacks>) in.readObject();
			System.out.println("The last key value of the map is :" + mapobject.size());
			// This condition used to iterate the loop based on number of products add to
			// the list
			for (int i = 1; i <= number; i++) {
				System.out.println("Enter the key value of Plans :");
				keyvalue = sc.nextInt();

				System.out.println("Enter the Offer name :");
				offerName = sc.nextInt();
				sc.nextLine();
				System.out.println("Enter the  Offer description :");
				offerDescription = sc.nextLine();

				System.out.println("Enter the Offer Validity:");
				validity = sc.nextInt();
				// This condition is used to add product object to map
				mapobject.put(keyvalue, new PostpaidPacks(offerName, offerDescription, validity));
				// This statements are used get the data from a particular File
				FileOutputStream file1 = new FileOutputStream(f);
				ObjectOutputStream out = new ObjectOutputStream(file1);
				// This statements are used to add the product to File
				out.writeObject(mapobject);
				// ObjectOutputStream close
				out.close();
				// File close
				file1.close();
				System.out.println("Plans are Added in the File ");
			}

		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("IOException is Exception");
		} catch (Exception ex) {
			ex.getMessage();
			System.out.println("You Enter the wrong input");
		}
	}

	/**
	 * This method is used to display what products are available in the file
	 * 
	 * @throws ClassNotFoundException when the file is not found
	 */
	// @SuppressWarnings("unchecked")
	public static int displayPostpaidPlans(int search) {
		int date = 0;
		try {

			// Reading the object from a file
			// Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream input = new ObjectInputStream(file);
			// Method for deserialization of object

			@SuppressWarnings("unchecked")
			Map<Integer, PostpaidPacks> mapobject = (Map<Integer, PostpaidPacks>) input.readObject();
			for (int key : mapobject.keySet()) {
				if (search == key) {
					PostpaidPacks product = mapobject.get(key);
					System.out.print("pack activated with RC " + product.getOfferName());
					System.out.print(" and got benefits : " + product.getOfferDescription());
					System.out.println();
					date = product.getValidity();

				}
			}
			// ObjectInputStream close
			input.close();
			// File close
			file.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("IOException is Exception");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("ClassNotFoundException");
		}
		return date;
	}

	/**
	 * this method is used for displaying the postpaid plans with the given key
	 */
	public static void displayPostpaid() {
		int count = 1;
		try {

			// Reading the object from a file
			// Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream input = new ObjectInputStream(file);
			// Method for deserialization of object

			@SuppressWarnings("unchecked")
			Map<Integer, PostpaidPacks> mapobject = (Map<Integer, PostpaidPacks>) input.readObject();
			for (int key : mapobject.keySet()) {

				PostpaidPacks product = mapobject.get(key);

				System.out.print(count + " recharge with  " + product.getOfferName());
				System.out.print(" and get : " + product.getOfferDescription());
				System.out.print("with Validity: " + product.getValidity() + "days");
				System.out.println();
				count++;
			}

			// ObjectInputStream close
			input.close();
			// File close
			file.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("IOException is Exception");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("ClassNotFoundException");
		}

	}

	/**
	 * this method is used to update the existing postpaid plans
	 */
	public static void updatePostpaidPlans() {

		try {
			System.out.println("Choose Update plans");
			displayPostpaid();
			int updateKey = sc.nextInt();
			// Reading the object from a file
			// Map<String, Plans> mapobject = (Map<String, Plans>) input.readObject();
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream input = new ObjectInputStream(file);
			// Method for deserialization of object

			@SuppressWarnings("unchecked")
			Map<Integer, PostpaidPacks> mapobject = (Map<Integer, PostpaidPacks>) input.readObject();
			for (int key : mapobject.keySet()) {
				if (updateKey == key) {
					PostpaidPacks product = mapobject.get(key);
					System.out.print("pack activated with RC " + product.getOfferName());
					System.out.print(" and got benefits : " + product.getOfferDescription());
					System.out.println();
					System.out.println("Enter the Offer name :");
					int offerName = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter the  Offer description :");
					String offerDescription = sc.nextLine();

					System.out.println("Enter the Offer Validity:");
					int validity = sc.nextInt();
					// This condition is used to add product object to map
					mapobject.put(updateKey, new PostpaidPacks(offerName, offerDescription, validity));
					// This statements are used get the data from a particular File
					FileOutputStream file1 = new FileOutputStream(fileName);
					ObjectOutputStream out = new ObjectOutputStream(file1);
					// This statements are used to add the product to File
					out.writeObject(mapobject);
					// ObjectOutputStream close
					out.close();
					System.out.println("Plans are Updated successfully ");

				}
			}

			// ObjectInputStream close
			input.close();
			// File close
			file.close();
			sc.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("IOException is Exception");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("ClassNotFoundException");
		}
		sc.close();
	}

}
